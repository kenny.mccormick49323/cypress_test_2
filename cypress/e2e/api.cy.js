/// <reference types="cypress" />


describe(`API Tests`, () => {

    var Authorization = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2ODY4MzQwNjQsImV4cCI6MTY4Njg4NDQ2NCwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiYWxpbmEub2trYW0rMUBnbWFpbC5jb20ifQ.lEpPoD7uW68rKi3YvnYDy8iH0FQzGuJw6gJMpyvYUmCaldVxGDLNykJ4tRmJJq1ZFRQP7L2YBNyQ_I1yLvkpJUfs-_If6E3JG8d7W6YELoYbmJUidWkkBNtAcL27ItefOEJi5z9gVH-E6C6_6Mp-5WhUtLgyMtzuYrLKIhkOO_8OQrqE0KRzs37aC6bLkxJpp2S2qYJwC9LlJbiLOiihKO-b3zT3jsVYIjT8FkdXmZyTthAEni871RNMrN5B5RVXjq0VXgOHC63opMf-6QLPoNMNv3uy-OmXauO7jZKH9XjDochdtyzh6K4O4J1RBe12Qx8XKw_CI4_3KnnSZaQ4oA'
    const mainUrl = "https://api.cat.market/api"

    before("Auth", () => {

        var email='alina.okkam+1@gmail.com'
        var password='123456'
        const url = mainUrl + "/v1/security/login";
        var data = { email: email, password: password };

        cy.request({
            method: 'POST',
            url: url,
            headers: {},
            body: data
        }).then((res)=>{
            expect(res.status).to.eq(200)
            return Authorization = 'Bearer ' + res.body.token
        })
    });

    it("Get user acc", () => {

        const url = mainUrl + "/v1/account";

        cy.request({
            method: 'GET',
            url: url,
            headers: {
                Authorization
            }
        }).then((res)=>{
            expect(res.status).to.eq(200)
        })
    });

  it("Data in merged_synthetic - Comparisation MaxDate", async () => {
    const url = mainUrl + "/v1/settings/ms/info";

    cy.request({
        method: 'GET',
        url: url,
        headers: {
            Authorization
        }
    }).then((res)=>{
        expect(res.statusText).to.eq("OK")

        var maxDate = response.data.maxDate; // maxDate YYYY-MM-DD in merged synthetic
        var today = new Date(); //actual date YYYY-MM-DD
        var currentDay = today.getDate() - 1; //actual DD minus 1 day 'cause the merged synthetic gathers data till previous date
        var currentDaystr = currentDay.toString();

        if (currentDaystr.length < 2) {
            // если цифра < 10 то присутствует 0 + число, если >= то ок
            var currentDaystr = "0" + currentDaystr;
            expect(maxDate.slice(8, 10)).to.equal(currentDaystr);
          } else {
            expect(maxDate.slice(8, 10)).to.equal(currentDaystr);
          };
    })
  });

  it("Acino UserAccount - all datas in selected category ", async () => {
    var categoryId = ["666", "667"]; 
    const url = mainUrl + "/v1/category/" + categoryId[0];

    cy.request({
        method: 'GET',
        url: url,
        headers: {
            Authorization
        }
    }).then((res)=>{
        expect(res.status).to.eq(200)
        console.log(res.data)
    })
  });


});
