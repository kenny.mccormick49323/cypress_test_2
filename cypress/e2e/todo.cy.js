/// <reference types="cypress" />
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe.only('Test', () => {
  // beforeEach( '', () => {
  //   // Cypress starts out with a blank slate for each test, so we must tell it to visit our website with the `cy.visit()` command.
  //   cy.visit('https://cat.market/signin')
  //   cy.url().should('include', '/signin')
  //   cy.title().should('include', 'ecom-project')
  // });

  it('Athorization', () => {

    cy.visit('https://cat.market/signin')
    cy.url().should('include', '/signin')
    cy.title().should('include', 'ecom-project')

    // We use the `cy.get()` command to get all elements that match the selector. Then, we use `should` to assert
    cy.get('.page__logo').should('be.visible')
    .should('have.attr', 'src', '/img/logo-cat.ac9229bd.png') 
    cy.get('.sign__title').should('have.length', 1)
    .should('contain.text', 'Вход в CAT')

    const email = 'alina.okkam@gmail.com'
    const password = '123456'

    const emailField = cy.get('input[type="text"]')
    .should('have.attr', 'placeholder', 'Эл. почта')
    .should('be.visible')

    emailField.type(`${email}`)
    .should("have.value", email)

    const eye = cy.get('.material-design-icon__svg')
    .should('be.visible')
    .should('have.attr', 'width', '24')
    eye.click()

    const passwolrField = cy.get('input[type="text"]').last()
    .should('have.attr', 'placeholder', 'Пароль')
    .should('be.visible')

    passwolrField.type(`${password}{enter}`)
    .should("have.value", password)

    // const button = cy.get('button[type="submit"] > .flex')
    // button.should('be.enabled').click()

    // var url = cy.url()
    // if (expect.soft(url).to.contain("welcome")) {
    //   console.log(`${url} = Welcome Screen`)
    // }else if (expect.soft(url).to.contain("settings")) {
    //   console.log(`${url} = Settings Screen`)
    // }

    // cy.url().should('include', '/welcome')
    // cy.url().should('include', '/settings')
    cy.url().should('include', '/dashboard')

  });


  it.skip('Welcome Screen', () => {

    cy.visit('https://cat.market/welcome')
    cy.url().should('include', '/welcome')

    const chooseCategory = cy.get(':nth-child(1) > .welcome__item-title')
    .should('be.visible')
    .should('contain.text', 'Выберите категорию')

    const GelShowerCategory = cy.get('.welcome__item-content > :nth-child(1)')
    .should('be.visible')
    .should('contain.text', 'Гели для душа')

    const faceCare = cy.get('.welcome__item-content > :nth-child(2)')
    .should('contain.text', 'Уход за лицом и телом')

    GelShowerCategory.click()

    cy.url().should('eql', 'https://cat.market/category/657/settings')
  });

  it('Settings Screen', () => {

    cy.visit('https://cat.market/category/657/settings')
    cy.url().should('include', '/settings')

    const settingsMenu = cy.get('.menu__title')
    .should('be.visible')
    .should('include.text', 'Настройки категории')

    const brandSearch = cy.get(':nth-child(1) > .category-filter > .category-filter__search > .category-filter__search-input')
    .should('be.visible')
    .should('have.attr', 'placeholder', 'Название бренда')

    var ourBrand = ['Palette', 'Syoss', 'Fa', 'Barnangen', 'Taft', 'GOT2B', 'Gliss Kur', 'Shamtu', 'Schauma', 'Perfect Mousse', 'Luminance', 'Men Perfect']

    brandSearch.type(`${ourBrand[9]}{enter}`)

    const checkbox_label = cy.get('.checkbox__label')
    .should('be.visible')
    // checkbox_label.first().click()

    var letter = cy.get('.category-filter__group-label').first()
    .should('be.visible')
    .should('include.text', `${ourBrand[9][0]}`)

    // for (let index = 0; index < ourBrand.length; index++) {
    //   var brand = ourBrand[index+1];
    //   brandSearch.type(`${brand}{enter}`)
    //   brandSearch.should('contain.text', brand)
    //   brandSearch.clear()
    // }

    var chosenBrands = cy.get(':nth-child(1) > .category-filter > .category-filter__chosen')
    .should('be.visible')
    chosenBrands.should('contain.text', 'PaletteSyossFaBarnangenTaftGOT2BGliss KurShamtuSchaumaPerfect MousseLuminanceMen Perfect')

    const saveSettingsButton =  cy.contains('Сохранить настройки и перейти к дашборду') //cy.get('.setting__save')
    .should('be.visible')

    saveSettingsButton.click()
    cy.url().should('include', '/dashboard')

    cy.pause()
    // https://cat.market/category/657/dashboard

  });

  

  
  //   // Let's get the input element and use the `type` command to
  //   // we need to type the enter key as well in order to submit the input.
  //   cy.get('[data-test=new-todo]').type(`${newItem}{enter}`)

  //   // Now that we've typed our new item, let's check that it actually was added to the list.
  //   // Since it's the newest item, it should exist as the last element in the list.
  //   // In addition, with the two default items, we should have a total of 3 elements in the list.
  //   // Since assertions yield the element that was asserted on,we can chain both of these assertions together into a single statement.
  //   cy.get('.todo-list li')
  //     .should('have.length', 3)
  //     .last()
  //     .should('have.text', newItem)
  // })

  // it('can check off an item as completed', () => {
  //   // In addition to using the `get` command to get an element by selector,
  //   // we can also use the `contains` command to get an element by its contents.
  //   // However, this will yield the <label>, which is lowest-level element that contains the text.
  //   // In order to check the item, we'll find the <input> element for this <label>
  //   // by traversing up the dom to the parent element. From there, we can `find`
  //   // the child checkbox <input> element and use the `check` command to check it.
  //   cy.contains('Pay electric bill')
  //     .parent()
  //     .find('input[type=checkbox]')
  //     .check()

  //   // Now that we've checked the button, we can go ahead and make sure
  //   // that the list element is now marked as completed.
  //   // Again we'll use `contains` to find the <label> element and then use the `parents` command
  //   // to traverse multiple levels up the dom until we find the corresponding <li> element.
  //   // Once we get that element, we can assert that it has the completed class.
  //   cy.contains('Pay electric bill')
  //     .parents('li')
  //     .should('have.class', 'completed')
  // })

  // context('with a checked task', () => {
  //   beforeEach(() => {
  //     // We'll take the command we used above to check off an element
  //     // Since we want to perform multiple tests that start with checking
  //     // one element, we put it in the beforeEach hook
  //     // so that it runs at the start of every test.
  //     cy.contains('Pay electric bill')
  //       .parent()
  //       .find('input[type=checkbox]')
  //       .check()
  //   })

  //   it('can filter for uncompleted tasks', () => {
  //     // We'll click on the "active" button in order to
  //     // display only incomplete items
  //     cy.contains('Active').click()

  //     // After filtering, we can assert that there is only the one
  //     // incomplete item in the list.
  //     cy.get('.todo-list li')
  //       .should('have.length', 1)
  //       .first()
  //       .should('have.text', 'Walk the dog')

  //     // For good measure, let's also assert that the task we checked off
  //     // does not exist on the page.
  //     cy.contains('Pay electric bill').should('not.exist')
  //   })

  //   it('can filter for completed tasks', () => {
  //     // We can perform similar steps as the test above to ensure
  //     // that only completed tasks are shown
  //     cy.contains('Completed').click()

  //     cy.get('.todo-list li')
  //       .should('have.length', 1)
  //       .first()
  //       .should('have.text', 'Pay electric bill')

  //     cy.contains('Walk the dog').should('not.exist')
  //   })

  //   it('can delete all completed tasks', () => {
  //     // First, let's click the "Clear completed" button
  //     // `contains` is actually serving two purposes here.
  //     // First, it's ensuring that the button exists within the dom.
  //     // This button only appears when at least one task is checked
  //     // so this command is implicitly verifying that it does exist.
  //     // Second, it selects the button so we can click it.
  //     cy.contains('Clear completed').click()

  //     // Then we can make sure that there is only one element
  //     // in the list and our element does not exist
  //     cy.get('.todo-list li')
  //       .should('have.length', 1)
  //       .should('not.have.text', 'Pay electric bill')

  //     // Finally, make sure that the clear button no longer exists.
  //     cy.contains('Clear completed').should('not.exist')
    // })
  // })
})
